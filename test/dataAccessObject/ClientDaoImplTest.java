package dataAccessObject;

import java.util.List;
import model.Client;
import model.Phone;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class ClientDaoImplTest {

    private Client client;
    private ClientDaoImpl daoImpl;
    private Phone phone;

    public ClientDaoImplTest() {
    }

//    @org.junit.Test
    public void testSave() {
        System.out.println("Salvar");
        client = new Client("Tiago", "871.445.967-44", "tiago@email.com");
        phone = new Phone("554898773464", "Movel", "Tim");
        client.setPhone(phone);
        daoImpl = new ClientDaoImpl();
        daoImpl.save(client);
    }

//    @org.junit.Test
    public void testList() {
        System.out.println("Listar");
        daoImpl = new ClientDaoImpl();
        List<Client> data = daoImpl.listAll();
        for (Client client : data) {
            System.out.println("ID: " + client.getId());
            System.out.println("Nome: " + client.getName());
            System.out.println("CPF: " + client.getIndividualRegistration());
            System.out.println("Email: " + client.getEmail());
            System.out.println("Telefone: " + client.getPhone().getPhoneNumber());
            System.out.println("Tipo: " + client.getPhone().getType());
            System.out.println("Operadora: " + client.getPhone().getPhoneCarrier());
            System.out.println(" ");
        }
    }

        @org.junit.Test
    public void testListById() {
        int id = 5;
        System.out.println("Listar por ID");
        daoImpl = new ClientDaoImpl();
        Client client = daoImpl.listById(id);
        System.out.println("ID: " + client.getId());
        System.out.println("Nome: " + client.getName());
        System.out.println("CPF: " + client.getIndividualRegistration());
        System.out.println("Email: " + client.getEmail());
        System.out.println("Telefone: " + client.getPhone().getPhoneNumber());
        System.out.println("Tipo: " + client.getPhone().getType());
        System.out.println("Operadora: " + client.getPhone().getPhoneCarrier());
        System.out.println(" ");
    }

//    @org.junit.Test
    public void testChange() {
        System.out.println("Alterar");
        client = new Client(5, "Tommy", "371.787.247-43", "tommy@email.com");
        phone = new Phone("554799660064", "Movel", "Vivo");
        phone.setId(5);
        client.setPhone(phone);
        daoImpl = new ClientDaoImpl();
        daoImpl.change(client);
    }

//    @org.junit.Test
    public void testDelete() {
        System.out.println("Deletar");
        client = new Client(1);
        daoImpl = new ClientDaoImpl();
        daoImpl.delete(client);
    }

//    @org.junit.Test
    public void testDeletePhone() {
        System.out.println("Deletar");
        phone = new Phone(2);
        daoImpl = new ClientDaoImpl();
        daoImpl.deletePhone(phone);
    }
}
