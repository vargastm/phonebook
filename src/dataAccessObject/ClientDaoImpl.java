package dataAccessObject;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Client;
import model.Phone;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class ClientDaoImpl implements Serializable {

    private Connection connection;
    private PreparedStatement prepared;
    private ResultSet result;
    private Client client;
    private PhoneDaoImpl phoneDaoImpl;

    public void save(Client client) {
        String query = "INSERT INTO client(name, individualRegistration, email) VALUES(?, ?, ?)";

        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            prepared.setString(1, client.getName());
            prepared.setString(2, client.getIndividualRegistration());
            prepared.setString(3, client.getEmail());
            prepared.executeUpdate();
            result = prepared.getGeneratedKeys();
            result.next();
            client.setId(result.getInt(1));
            phoneDaoImpl = new PhoneDaoImpl();
            phoneDaoImpl.save(client.getPhone(), client.getId(), connection);
        } catch (Exception e) {
            System.out.println("Erro ao gravar cliente" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    public List<Client> listAll() {
        String query = "SELECT * FROM client LEFT JOIN phone ON client.idclient = phone.idclient";
        List<Client> data = new ArrayList<>();
        try {
            Phone phone;
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            result = prepared.executeQuery();
            while (result.next()) {
                client = new Client();
                client.setId(result.getInt("idclient"));
                client.setName(result.getString("name"));
                client.setIndividualRegistration(result.getString("individualRegistration"));
                client.setEmail(result.getString("email"));
                data.add(client);

                phone = new Phone();
                phone.setId(result.getInt("idphone"));
                phone.setPhoneNumber(result.getString("phoneNumber"));
                phone.setType(result.getString("type"));
                phone.setPhoneCarrier(result.getString("phoneCarrier"));
                client.setPhone(phone);
                data.add(client);
            }
        } catch (Exception e) {
            System.out.println("Erro ao listar todos os telefones" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
        return data;
    }

    public Client listById(int id) {
        String query = "SELECT * FROM client INNER JOIN phone ON client.idclient = phone.idclient WHERE client.idclient = ?";
        try {
            Phone phone;
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setInt(1, id);
            result = prepared.executeQuery();
            client = new Client();
            result.next();
            client.setId(result.getInt("idclient"));
            client.setName(result.getString("name"));
            client.setIndividualRegistration(result.getString("individualRegistration"));
            client.setEmail(result.getString("email"));

            phone = new Phone();
            phone.setId(result.getInt("idphone"));
            phone.setPhoneNumber(result.getString("phoneNumber"));
            phone.setType(result.getString("type"));
            phone.setPhoneCarrier(result.getString("phoneCarrier"));
            client.setPhone(phone);
        } catch (Exception e) {
            System.out.println("Erro ao pesquisar cliente por id" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
        return client;
    }

    public void change(Client client) {
        String query = "UPDATE client SET name = ?, individualRegistration = ?, email = ? WHERE idclient = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setString(1, client.getName());
            prepared.setString(2, client.getIndividualRegistration());
            prepared.setString(3, client.getEmail());
            prepared.setInt(4, client.getId());
            prepared.executeUpdate();
            phoneDaoImpl = new PhoneDaoImpl();
            phoneDaoImpl.change(client.getPhone(), connection);
        } catch (Exception e) {
            System.out.println("Erro ao alterar cliente" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    public void delete(Client client) {
        String query = "DELETE FROM client WHERE idclient = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setInt(1, client.getId());
            prepared.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro ao deletar cliente" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }

    public void deletePhone(Phone phone) {
        String query = "DELETE FROM phone WHERE idphone = ?";
        try {
            connection = ConnectionFactory.getConnection();
            prepared = connection.prepareStatement(query);
            prepared.setInt(1, phone.getId());
            prepared.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro ao deletar telefone" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }
}
