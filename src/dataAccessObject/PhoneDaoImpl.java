package dataAccessObject;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import model.Phone;

/**
 *
 * @author Tiago Martins Vargas
 */
public class PhoneDaoImpl implements Serializable {
    
    private PreparedStatement prepared;
    
    public void save (Phone phone, int id, Connection connection) {
        String query =  "INSERT INTO phone(phoneNumber, type, phoneCarrier, idclient) VALUES(?, ?, ?, ?)";
    
        try {
            prepared = connection.prepareStatement(query);
            prepared.setString(1, phone.getPhoneNumber());
            prepared.setString(2, phone.getType());
            prepared.setString(3, phone.getPhoneCarrier());
            prepared.setInt(4, id);
            prepared.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro ao gravar telefone" + e.getMessage());
        } 
    }    
    
        public void change (Phone phone, Connection connection) {
        String query = "UPDATE phone SET phoneNumber = ?, type = ?, phoneCarrier = ? WHERE idphone = ?";
        try {
            prepared = connection.prepareStatement(query);
            prepared.setString(1, phone.getPhoneNumber());
            prepared.setString(2, phone.getType());
            prepared.setString(3, phone.getPhoneCarrier());
            prepared.setInt(4, phone.getId());
            prepared.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro ao alterar telefone" + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(connection, prepared);
        }
    }
}
