package model;

/**
 *
 * @author Tiago Martins Vargas
 * 
 */
public class Phone {
    
    private Integer id;
    private String phoneNumber;
    private String type;
    private String phoneCarrier;

    public Phone() {
    }

    public Phone(Integer id) {
        this.id = id;
    }
    
    
    public Phone(String phoneNumber, String type, String phoneCarrier) {
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.phoneCarrier = phoneCarrier;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneCarrier() {
        return phoneCarrier;
    }

    public void setPhoneCarrier(String phoneCarrier) {
        this.phoneCarrier = phoneCarrier;
    }
}
