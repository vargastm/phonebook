package model;

/**
 *
 * @author Tiago Martins Vargas
 * 
 */
public class Client {
    
    private Integer id;
    private String name;
    private String individualRegistration; // indivudal registration used for paying income tax // CPF (Brazil).
    private String email;
    private Phone phone;

    public Client() {
    }

    public Client(Integer id) {
        this.id = id;
    }

    public Client(Integer id, String name, String individualRegistration, String email) {
        this.id = id;
        this.name = name;
        this.individualRegistration = individualRegistration;
        this.email = email;
    }

    public Client(String name, String individualRegistration, String email) {
        this.name = name;
        this.individualRegistration = individualRegistration;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndividualRegistration() {
        return individualRegistration;
    }

    public void setIndividualRegistration(String individualRegistration) {
        this.individualRegistration = individualRegistration;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }
}
